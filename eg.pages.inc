<?php

/**
 * @file
 * Main file for the eg module, contains functions for interacting with the
 * Excellence Gateway web service.
 */

/**
 * Generates some content for the demonstration metadata page.
 *
 * This is probably now de-funked and likely to go away.
 *
 * @return string
 *   The markup of the metadata page, used by eg_menu() function.
 */
function eg_show_metadata_page() {

  $build = array();

  $metadata = "<record>
<general>
<identifier>
<catalog>uri</catalog>
<entry>eg:1234</entry>
</identifier>
<title>
Complex needs, complex choices - The role of Career Learning, Information Advice and Guidance in the Further Education and Skills sector
</title>
<languages>
<language>en-gb</language>
</languages>
<description>
This report presents the findings of research undertaken by CFE on behalf of the Learning and Skills Improvement Service (LSIS) and National Union of Students (NUS) to develop a more sophisticated understanding of Career Learning, Information, Advice and Guidance (CLIAG) for learners engaged within the Further Education (FE) and Skills sector.
</description>
<geographical-coverage>
<region>national</region>
</geographical-coverage>
</general>
<lifecycle>
<version>1</version>
<status>final</status>
<contributions>
<contribution>
<role>author</role>
<entity>Neil Allison</entity>
</contribution>
</contributions>
</lifecycle>
<meta-metadata>
<identifier>
<catalog>metadata-internal</catalog>
<entry>250438</entry>
</identifier>
<contributions>
<contribution>
<role>creator</role>
<entity>Neil Allison (nallison)</entity>
<date>2012-04-05</date>
</contribution>
</contributions>
<metadata-schema>http://www.qia.org.uk/schemes/EG1_0</metadata-schema>
<metadata-language>en-gb</metadata-language>
</meta-metadata>
<technical>
<location>eg:1234</location>
<alternative-locations>
<location>eg:1234</location>
</alternative-locations>
<installation-remarks/>
<other-platform-requirements/>
</technical>
<educational/>
<rights>
<has-cost>no</has-cost>
<has-use-restrictions>yes</has-use-restrictions>
<use-restrictions/>
<copyright-location/>
</rights>
<classification>
<taxon-path>
<source>EGmediaType</source>
<taxon>
<id>013643</id>
<entry>Report</entry>
</taxon>
</taxon-path>
<taxon-path>
<source>EGpublisher</source>
<taxon>
<id>020087</id>
<entry>LSIS</entry>
</taxon>
</taxon-path>
<taxon-path>
<source>EGsupportservices</source>
<taxon>
<id>020006</id>
<entry>Career Development</entry>
</taxon>
</taxon-path>
</classification>
</record>";

  $xsl_content = variable_get('eg_metadata_xsl', '');

  if ($xsl_content == '') {
    $xsl_content = eg_default_xsl();
  }

  $xml = new DOMDocument();
  $xml->loadXML($metadata);

  $xsl = new DOMDocument();
  $xsl->loadXML($xsl_content);

  $proc = new XSLTProcessor();
  $proc->importStyleSheet($xsl);

  $html = $proc->transformToXML($xml);

  $build['no_content'] = array(
    '#markup' => $html,
  );

  return $build;
}
