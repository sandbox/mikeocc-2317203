<?php

/**
 * @file
 * Admin interface for the eg module.
 */

/**
 * Implements hook_config_form().
 */
function eg_config_form(array $form, array &$form_state, array $environment = array()) {

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Excellence Gateway web service URL'),
    '#default_value' => variable_get("eg_web_service_url", "http://api.excellencegateway.org.uk/"),
    '#description' => t('e.g. http://api.excellencegateway.org.uk/'),
    '#required' => TRUE,
  );

  $form['xsl'] = array(
    '#type' => 'textarea',
    '#title' => t('Metadata display transformation'),
    '#default_value' => variable_get("eg_metadata_xsl", eg_default_xsl()),
    '#description' => t('A complete XSL document describing the transformation from metadata to displayed HTML'),
    '#required' => FALSE,
  );

  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#submit' => array('eg_config_form_submit'),
    '#value' => t('Save'),
  );

  // Ensures destination is an internal URL, builds "cancel" link.
  if (isset($_GET['destination']) && !url_is_external($_GET['destination'])) {
    $destination = $_GET['destination'];
  }
  else {
    $destination = '<front>';
  }
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $destination,
  );

  return $form;
}

/**
 * Implements hoo_config_form_submit().
 */
function eg_config_form_submit(array $form, array &$form_state) {
  variable_set("eg_web_service_url", $form_state['values']['url']);
  variable_set("eg_metadata_xsl", $form_state['values']['xsl']);
}
