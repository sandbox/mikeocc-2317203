Excellence Gateway Read API
===========================

-   [General concepts](#general)
-   [Entity descriptions and discovery](#entities)
    -   [Content resources](#content)
    -   [Search Suggestion](#search-suggest)
    -   [Metadata](#metadata)
    -   [Vocabulary](#vocabs)
-   [SOLR search schema](#solr-schema)

The Excellence Gateway Read API provides a simple, REST-like way to access the
resources on the Excellence Gateway. It provides metadata about each resource
and search functionality to discover relevant resources.

<a id="general">General concepts</a>
------------------------------------

The Excellence Gateway is a repository of resources. These are categorized
according to several controlled vocabularies of terms. Resources are either
documents that can be downloaded, or small self-contained online resources that
can be accessed via a web browsed. Documents are described by a set of
metadata, and searches against content and against metadata are available using
a standard SOLR search URL syntax.

The design of the read API is REST-like, so the sections following describe the
entity types that can be accessed, and the syntax of the URLs for their
discovery.

<a id="entities">Entity types</a>
---------------------------------

###<a id="content">Content resources</a>
#### Description

Content resources are either downloadable documents or landing pages for small,
self-contained, online resources. As such, they can be of many different media
types, but all of them should be suitable for direct browsing by users.

#### Discovery

Discovery of content resources is by a SOLR-based search. The documentation for
SOLR can be found here:

<http://lucene.apache.org/solr/documentation.html>

To use the Excellence Gateway SOLR search, you should send a GET request to the
following URL:

    GET /resources?mode=solr&...

After the first parameter, you may include all the parameters for a standard
SOLR search. The common search parameters described in the SOLR reference are
all accepted along with those for a DisMax query parser.

The results will be in the XML format described in the SOLR reference guide.
See the [SOLR schema section](#solr-schema) for details on the fields in the
Excellence Gateway index. In particular the url and metadata-url fields are the
locations of the resource and its metadata.

##### Example request

    GET /resources?mode=solr&q=esol 16-18&fq=EGresourcetype:("Research study") AND
     EGsupportservices:("Skills for Life support")&fl=title,description,moddate,url&
     start=0&sort=score desc&qf=title^2 description^1.5 content^1&pf=title^2
     description^1.5 content^1&q.alt=*:*&ps=1&rows=10

The above query is a text search for the term "ESOL 16-18" with faceted search
restrictions on resource type and service area. The requested fields are title,
description, modification date, and URL. The results are sorted by relevance
and limited to 10 items per page. In calculating the relevance score, some
weightings are given to particular fields and similarly to whole phrase matches
within those fields.

##### Example response

    <?xml version="1.0" encoding="UTF-8"?>
    <response>
    <lst name="responseHeader">
       <int name="status">0</int>
       <int name="QTime">1</int>
       <lst name="params">
          <str name="q">esol</str>
          ...
       </lst>
    </lst>

    <result name="response" numFound="4" start="0">

       <doc>
          <str name="description">This starter kit provides support and guidance,
           information and resources for developing and delivering ESOL (English for
           speakers of other languages). It includes the development of blended
           learning approaches.</str>
          <date name="moddate">2012-05-23T10:54:13Z</date>
          <str name="title">ESOL Starter Kit</str>
          <str name="url">http://repository.excellencegateway.org.uk/...</str>
          ...
       </doc>

       <doc>
       ...

###<a id="search-suggest">Search Suggestion</a>

#### Description

If you are implementing a search page for users the read api provides support
for search suggestions. The api is designed to be usable by a tool such as
[jQuery autocomplete](http://jqueryui.com/autocomplete "jQuery Autocomplete").
The search suggestions are based on the resource title.

##### Example request

    GET /searchsuggest?q=edu&callback=myCallback&rows=6

If you require facet restrictions then these can be supplied with something
like:

    GET /searchsuggest?q=edu&callback=myCallback&rows=6
     &EGgeogcoverage=["East+of+England"+"South+West"]
     &EGservices=["Best+practice"]

However, if you do supply facet restrictions you will have to take
responsibility for ensuring consistency across the restrictions passed to the
search suggestion, the final search and any user interface elements on the
page. How you do this will depend upon the design of your search page.

##### Example response

    myCallback({"results":["education","educational","education:","educator",
     "educ","educaplay","educare","education)"]})

###<a id="metadata">Metadata</a>

#### Description

The metadata stored about each content resource can be obtained as an XML
document.

##### Example response

    <record>
       <general>
          <title>Janet</title>
          <languages>
             <language>en-gb</language>
          </languages>
          <description>
             Janet is a world-leading network that was established over 25 years ago
              to service the requirements of the research and education sectors in the
              UK. The Janet network connects over 18 million end users in
              universities, colleges of higher and further education, research
              councils, schools and museums who collectively form the Janet community.
              We have developed the network to enable the community to connect,
              collaborate and provide innovative ways to support teaching and
              learning.
          </description>
       </general>
       <lifecycle>
          <version>1</version>
          <status>final</status>
          <contributions>
             <contribution>
                <role>author</role>
                <entity>John Smith</entity>
             </contribution>
          </contributions>
       </lifecycle>
       <meta-metadata>
          <identifier>
          <catalog>metadata-internal</catalog>
          <entry>249283</entry>
          </identifier>
          <contributions>
             <contribution>
                <role>creator</role>
                <entity>John Smith</entity>
                <date>2011-11-07</date>
             </contribution>
          </contributions>
          <metadata-schema>http://www.qia.org.uk/schemes/EG1_0</metadata-schema>
          <metadata-language>en-gb</metadata-language>
       </meta-metadata>
       <technical>
          <location>http://repository.excellencegateway.org.uk/...</location>
       </technical>
       <rights>
          <has-cost>no</has-cost>
          <has-use-restrictions>yes</has-use-restrictions>
          <use-restrictions/>
          <copyright-location/>
       </rights>
       <classification>
          <taxon-path>
          <source>EGaudience</source>
          <taxon>
          <id>020100</id>
          <entry>Practitioners</entry>
          </taxon>
          <taxon>
          <id>020097</id>
          <entry>Managers</entry>
          </taxon>
          </taxon-path>
          <taxon-path>
          <source>EGgeogcoverage</source>
          <taxon>
          <id>020004</id>
          <entry>UK-wide</entry>
          </taxon>
          </taxon-path>
          <taxon-path>
          <source>EGinfoarch2011</source>
          <taxon>
          <id>020108</id>
          <entry>Support for improvement</entry>
          </taxon>
          </taxon-path>
       </classification>
    </record>
<table>
  <colgroup>
    <col width="33%" />
    <col width="33%" />
    <col width="33%" />
  </colgroup>
  <thead>
    <tr class="header">
      <th align="left">Field name</th>
      <th align="left">Type</th>
      <th align="left">Description</th>
    </tr>
  </thead>
  <tbody>
  <tr class="odd">
    <td>general/title</td>
    <td>String</td>
    <td>The title of the resource</td>
  </tr>
  <tr>
    <td>general/languages/language</td>
    <td>String</td>
    <td>A language this resource is available in</td>
  </tr>

  <tr>
    <td>general/description</td>
    <td>String</td>
    <td>A short description of the resource</td>
  </tr>

  <tr>
    <td>lifecycle/version</td>
    <td>Number</td>
    <td>A version number for the resource</td>
  </tr>

  <tr>
    <td>lifecycle/status</td>
    <td>String</td>
    <td>The status of the resource. Either 'draft' or 'final'</td>
  </tr>

  <tr>
    <td>lifecycle/contributions/contribution/role</td>
    <td>String</td>
    <td>A type of contribution, 'author', 'publisher' etc.</td>
  </tr>

  <tr>
    <td>lifecycle/contributions/contribution/entity</td>
    <td>String</td>
    <td>The person or entity responsible for the contribution</td>
  </tr>

  <tr>
    <td>technical/location</td>
    <td>String</td>
    <td>A URL from which the resource can be fetched</td>
  </tr>

  <tr>
    <td>technical/size</td>
    <td>Number</td>
    <td>For downloadable documents, the size of the file in bytes</td>
  </tr>

  <tr>
    <td>technical/filename</td>
    <td>String</td>
    <td>For downloadable documents, the original file name</td>
  </tr>

  <tr>
    <td>rights/has-cost</td>
    <td>Boolean</td>
    <td>False when the resource is free to use, true otherwise</td>
  </tr>

  <tr>
    <td>rights/has-use-restrictions</td>
    <td>Boolean</td>
    <td>True when restrictions exist on the use of the resource, false otherwise</td>
  </tr>

  <tr>
    <td>rights/use-restrictions</td>
    <td>String</td>
    <td>A description of the restrictions on use</td>
  </tr>

  <tr>
    <td>rights/copyright-location</td>
    <td>String</td>
    <td>A URL from which copyright details can be fetched</td>
  </tr>

  <tr>
    <td>rights/copyright-location</td>
    <td>String</td>
    <td>A URL from which copyright details can be fetched</td>
  </tr>

  <tr>
    <td>classifications/taxon-path/source</td>
    <td>String</td>
    <td>The identifier for a vocabulary from which all taxon tags within this taxon-path are drawn.</td>
  </tr>

  <tr>
    <td>classifications/taxon-path/taxon/id</td>
    <td>String</td>
    <td>The identifier for a taxonomic term that has been applied to this resource.</td>
  </tr>

  <tr>
    <td>classifications/taxon-path/taxon/entry</td>
    <td>String</td>
    <td>The name of the taxonomic term that has been applied.</td>
  </tr>

  </tbody>
</table>

#### Discovery

Discovery of metadata is also via SOLR-based search. Use the approach described
in the content resources discovery section above.

###<a id="vocabs">Vocabulary</a>

#### Description

The content of the Excellence Gateway is organized by a set of controlled vocabularies. These can be obtained via the read API.

##### Example response

<vocab id="EGssa">
<term id="5" name="Construction, Planning and the Built Environment">
<term id="5.1" name="Architecture"/>
<term id="5.2" name="Building and Construction"/>
<term id="5.3" name="Urban, Rural and Regional Planning"/>
</term>
<term id="13" name="Education and Training">
<term id="13.2" name="Direct Learning Support"/>
<term id="13.1" name="Teaching and Lecturing"/>
</term>
<term id="9" name="Arts, Media and Publishing">
<term id="9.2" name="Crafts, Creative Arts and Design"/>
<term id="9.3" name="Media and Communication"/>
<term id="9.1" name="Performing Arts"/>
<term id="9.4" name="Publishing and Information Services"/>
</term>
</vocab>

#### Discovery

A list of current vocabularies can be found by sending a GET request to:

GET /vocabularies

##### Example response

<ArrayOfstring xmlns:i="http://www.w3.org/2001/XMLSchema-instance"
xmlns="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
<string>http://api.excellencegateway.org.uk/vocabularies/EGcoverage</string>
<string>http://api.excellencegateway.org.uk/vocabularies/EGregions</string>
</ArrayOfstring>

<a id="solr-schema">SOLR schema</a>
-----------

The SOLR index for the Excellence Gateway contains the following fields of interest:
<table>
  <colgroup>
    <col width="33%" />
    <col width="33%" />
    <col width="33%" />
  </colgroup>
  <thead>
    <tr class="header">
      <th align="left">Field name</th>
      <th align="left">Type</th>
      <th align="left">Description</th>
    </tr>
  </thead>
  <tbody>

  <tr>
    <td>url</td>
    <td>String</td>
    <td>The URL of the content resource. It can be assumed that users may browse directly to this URL</td>
  </tr>
  <tr>
    <tr>
      <td>metadata-url</td>
      <td>String</td>
      <td>The URL from which the metadata for this resource may be retrieved</td>
    </tr>
    <tr>
      <td>pid</td>
      <td>String</td>
      <td>A unique identifier for the resource</td>
    </tr>
    <tr>
      <td>title</td>
      <td>Text</td>
      <td>The title of the resource</td>
    </tr>
    <tr>
      <td>description</td>
      <td>Text</td>
      <td>A short description of the resource</td>
    </tr>
    <tr>
      <td>moddate</td>
      <td>Date</td>
      <td>The last modification date of the resource</td>
    </tr>
    <tr>
      <td>create-date</td>
      <td>Date</td>
      <td>The creation date of the resource</td>
    </tr>
    <tr>
      <td>[taxonomic fields]</td>
      <td>Multi-string</td>
      <td>One field per indexed vocabulary. The field name matches the name of
        the vocabulary and contains the term identifier for each term within the
        vocabulary that this resource was tagged with</td>
    </tr>
    <tr>
      <td>archived</td>
      <td>Boolean</td>
      <td>When true, this resource is archived and may be outdated</td>
    </tr>
    <tr>
      <td>filesize</td>
      <td>Number</td>
      <td>For downloadable documents, the size of the file in bytes</td>
    </tr>
    <tr>
      <td>filename</td>
      <td>String</td>
      <td>For downloadable documents, the original file name</td>
    </tr>

    </tbody>
  </table>





