<?php

/**
 * @file
 * Provides basic integration with the Excellence Gateway repository
 */

/**
 * Implements hook_menu().
 */
function eg_menu() {
  $items = array();
  $items['admin/config/services/eg'] = array(
    'title'              => 'Excellence Gateway web service',
    'description'        => 'Set the URL for Excellence Gateway web service.',
    'page callback'      => 'drupal_get_form',
    'page arguments'     => array('eg_config_form'),
    'access arguments'   => array('administer site configuration'),
    'weight'             => -8,
    'file'               => 'eg.admin.inc',
  );

  $items['eg/metadata'] = array(
    'type' => MENU_CALLBACK,
    'page callback'      => 'eg_show_metadata_page',
    'access arguments'   => array('access content'),
    'file'               => 'eg.pages.inc',
  );

  return $items;
}

/**
 * The base URL of the Excellence Gateway API.
 *
 * This url is set on the configuration page for the Excellence Gateway web
 * service.
 */
function eg_get_webservice_url() {
  return variable_get("eg_web_service_url", "http://api.excellencegateway.org.uk/");
}

/**
 * The URL of the resources method of the Excellece Gateway web service.
 *
 * @return string
 *   The base url of the Excellence Gateway web service.
 */
function eg_get_webservice_resources_url() {
  return eg_get_webservice_url() . "resources";
}

/**
 * The URL of the vocabularies method of the Excellece Gateway web service.
 *
 * @return string
 *   The URL of the vocabularies method.
 */
function eg_get_webservice_vocabularies_url() {
  return eg_get_webservice_url() . "vocabularies";
}

/**
 * The URL of the search suggestion method of the Excellece Gateway web service.
 *
 * @return string
 *   The URL of the search suggestion method.
 */
function eg_get_webservice_search_suggest_url() {
  return eg_get_webservice_url() . "searchsuggest";
}

/**
 * Returns an array of URLs, one for each vocabulary.
 *
 * The URLs returned can be used to request full details for each vocabulary.
 * The details are returned in XML format describing the hierarchy of terms.
 *
 * @return array
 *   An array of URLs.
 */
function eg_get_vocabularies() {
  $url = eg_get_webservice_vocabularies_url();

  $options = array();
  // Always GET for now - may need POST if URL gets very long.
  $options['method'] = 'GET';

  $result = drupal_http_request($url, $options);

  if (!isset($result->code) || $result->code < 0) {
    $result->code = 0;
    $result->status_message = 'Request failed';
    $result->protocol = 'HTTP/1.0';
  }

  // Additional information may be in the error property.
  if (isset($result->error)) {
    $result->status_message .= ': ' . check_plain($result->error);
  }

  if (!isset($result->data)) {
    $result->data = '';
    $result->response = NULL;
  }
  else {
    $array_data = (array) simplexml_load_string($result->data);
    $result->response = $array_data['string'];

  }
  return $result->response;
}

/**
 * Returns a hierarchical structure describing a single vocabulary.
 *
 * @param string $url
 *   One of the urls returned by eg_get_vocabularies().
 *
 * @return array
 *   An array describing the hierarchy of a single vocabulary.
 */
function eg_get_vocabulary($url) {
  $options = array();

  // Always GET for now - may need POST if URL gets very long.
  $options['method'] = 'GET';

  $result = drupal_http_request($url, $options);

  if (!isset($result->code) || $result->code < 0) {
    $result->code = 0;
    $result->status_message = 'Request failed';
    $result->protocol = 'HTTP/1.0';
  }

  // Additional information may be in the error property.
  if (isset($result->error)) {
    $result->status_message .= ': ' . check_plain($result->error);
  }

  if (!isset($result->data)) {
    return NULL;
  }
  else {
    $vocab_object = simplexml_load_string($result->data);

    $root_terms = array();
    foreach ($vocab_object->term as $term) {
      $root_terms[] = _eg_process_term($term);
    }
    return $root_terms;
  }
}

/**
 * This is a "Private" recursive function used by eg_get_vocabulary().
 *
 * @param string $term_xml_object
 *   Term information.
 *
 * @return array
 *   Term information.
 */
function _eg_process_term($term_xml_object) {
  $term = array();

  $attributes = $term_xml_object->attributes();

  $term['id'] = (string) $attributes->id;
  $term['name'] = (string) $attributes->name;

  $term['narrower_terms'] = array();

  if (isset($term_xml_object->term)) {
    foreach ($term_xml_object->term as $narrower_term) {
      $term['narrower_terms'][] = _eg_process_term($narrower_term);
    }
  }
  return $term;
}

/**
 * Get a list of counts for a vocabulary.
 *
 * @param string $facet_name
 *   The name of a vocabulary to retrieve the facet counts for.
 *
 * @return array
 *   An indexed array of Term:Term (count)
 */
function eg_get_facet_values($facet_name) {
  // Query without filters and rows=0 to get all facet counts.
  $result = eg_get_resources_solr(array(
    'facet' => 'true',
    'facet.field' => $facet_name,
    'rows' => '0',
    'facet.sort' => 'index')
  );
  $facet_counts = $result->response->facet_counts->facet_fields->{$facet_name};

  $options = array();

  // Awkward processing because the array is alternating string values and
  // integer counts.
  for ($i = 0; $i <= count($facet_counts) - 2; $i += 2) {
    $options[$facet_counts[$i]] = $facet_counts[$i]
      . " (" . $facet_counts[$i + 1] . ")";
  }
  return $options;
}

/**
 * Build a SOLR query from the parameters and pass to the EG webservice.
 *
 * @param array $params
 *   An array of key => value pairs corresponding to the query parameters of a
 *   SOLR search.
 *
 * @return object
 *   An object of the form returned by the drupal_http_request with the response
 *   modified to hold the decoded json result of the query.  If no data
 *   available the response is null.
 *
 * @see drupal_http_request()
 */
function eg_get_resources_solr($params) {
  $url = eg_get_webservice_resources_url() . "?mode=solr&qf=title^2%20description^1.5%20content^1&pf=title^2%20description^1.5%20content^1&qt=dismax&wt=json&q.alt=*:*&ps=1";

  foreach ($params as $key => $value) {
    if (is_array($value)) {
      foreach ($value as $individual_value) {
        $url .= "&" . urlencode($key) . "=" . urlencode($individual_value);
      }
    }
    else {
      $url .= "&" . urlencode($key) . "=" . urlencode($value);
    }
  }

  $options = array();
  // Always GET for now - may need POST URLs get too long.
  $options['method'] = 'GET';

  $result = drupal_http_request($url, $options);

  if (!isset($result->code) || $result->code < 0) {
    $result->code = 0;
    $result->status_message = 'Request failed';
    $result->protocol = 'HTTP/1.0';
  }

  // Additional information may be in the error property.
  if (isset($result->error)) {
    $result->status_message .= ': ' . check_plain($result->error);
  }

  if (!isset($result->data)) {
    $result->data = '';
    $result->response = NULL;
  }
  else {
    $result->response = json_decode($result->data);
  }
  return $result;
}

/**
 * Returns the default xsl.
 *
 * @return string
 *   The default xsl.
 */
function eg_default_xsl() {
  return <<<EOT
  <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" version="1.0">
  <xsl:output method="html"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <h2>
    <xsl:value-of select="record//general//title"/>
    </h2>
    <p>
    <xsl:value-of select="record//general//description"/>
    </p>

  </xsl:template>
  </xsl:stylesheet>
EOT;
}
